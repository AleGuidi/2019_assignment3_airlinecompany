package com.assignment3.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Hostess extends Staff{

	@ManyToMany(cascade = CascadeType.ALL)
	private List<Plane> planes;
	
	public Hostess() {
		super();
	}
	public Hostess(String Name, String LastName, String Nationality) {
		super(Name, LastName, Nationality);
	}

	public List<Plane> getPlanes() {
		return planes;
	}

	public void setPlanes(List<Plane> planes) {
		this.planes = planes;
	}
 
}
