package com.assignment3.demo.model;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Pilot extends Staff{

	private Integer YearsService;
	private String TypeAircraft;
	
	@OneToMany(mappedBy = "captains")
	private List<Plane> planes;
	
	public Pilot() {
		super();
	}
	public Pilot(String Name, String LastName, String Nationality, int YearsService, String TypeAircraft) {
		super(Name, LastName, Nationality);
		this.YearsService = YearsService;
		this.TypeAircraft = TypeAircraft;
	}
	public Integer getYearsService() {
		return YearsService;
	}
	public void setYearsService(Integer yearsService) {
		YearsService = yearsService;
	}
	public String getTypeAircraft() {
		return TypeAircraft;
	}
	public void setTypeAircraft(String typeAircraft) {
		TypeAircraft = typeAircraft;
	}
	
	public List<Plane> getPlanes() {
		return planes;
	}
	public void setPlanes(List<Plane> planes) {
		this.planes = planes;
}
	
}