package com.assignment3.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Airport {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer code;
	private String name;
	private String Nation;
	private String City;
	

	@ManyToMany(cascade = {CascadeType.ALL})
	private List<Register> registers;
	
	@ManyToOne
	private Airport departure;
	
	@OneToMany(mappedBy = "departure")
	private List<Airport> Destinations;
	
	public Airport() {
		super();
	}
	public Airport(String name, String nation, String city) {
		super();
		this.name = name;
		Nation = nation;
		City = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNation() {
		return Nation;
	}
	public void setNation(String nation) {
		Nation = nation;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public List<Register> getRegisters() {
		return registers;
	}
	public void setRegisters(List<Register> registers) {
		this.registers = registers;
	}
	public Airport getDeparture() {
		return departure;
	}
	public void setDeparture(Airport departure) {
		this.departure = departure;
	}
	public List<Airport> getDestinations() {
		return Destinations;
	}
	public void setDestinations(List<Airport> destinations) {
		Destinations = destinations;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	
}