package com.assignment3.demo.model;


import java.util.List;

//import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Plane {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer code;
	private String Type;
	private Integer NumberSeats;
	
	@ManyToOne
	private Pilot captains;
	
	@ManyToMany(mappedBy = "planes")
	private List<Hostess> hostess;
	
	@OneToMany(mappedBy = "plane")
	private List<Register> registers;
	
	public Plane() {
		super();
	}
	public Plane(String type, int numberSeats, Pilot captains, List<Hostess> hostess) {
		super();
		Type = type;
		NumberSeats = numberSeats;
		this.captains = captains;
		this.hostess = hostess;
	}

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public Integer getNumberSeats() {
		return NumberSeats;
	}

	public void setNumberSeats(Integer numberSeats) {
		NumberSeats = numberSeats;
	}
	public Pilot getCaptains() {
		return captains;
	}
	public void setCaptains(Pilot captains) {
		this.captains = captains;
	}
	public List<Hostess> getHostess() {
		return hostess;
	}
	public void setHostess(List<Hostess> hostess) {
		this.hostess = hostess;
	}
	public List<Register> getRegisters() {
		return registers;
	}
	public void setRegisters(List<Register> registers) {
		this.registers = registers;
	}
		
}
	