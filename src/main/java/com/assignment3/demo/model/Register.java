package com.assignment3.demo.model;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Register{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer code;
	private String arrivalDate;
	private String arrivalTime;
	private String departureDate;
	private String departureTime;
	
	@ManyToOne
	private Plane plane;
	
	@ManyToMany(mappedBy = "registers")
	private List<Airport> airports;
	
	
	public Register() {
		super();
	}

	public Register (String departureDate, String departureTime, String arrivalDate,String arrivalTime, Plane plane, List<Airport> airpots) {
		super();
		this.arrivalDate = arrivalDate;
		this.arrivalTime = arrivalTime;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.plane=plane;
		this.airports=airpots;
		//this.code=code;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}
	
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public String getDepartureDate() {
		return departureDate;
	}
	
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getDepartureTime() {
		return departureTime;
	}
	
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public List<Airport> getAirports() {
		return airports;
	}
	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Plane getPlane() {
		return plane;
	}

	public void setPlane(Plane plane) {
		this.plane = plane;
	}

}
