package com.assignment3.demo.model;

import javax.persistence.*;

@Entity
public abstract class Staff {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer code;
	private String Name;
	private String LastName;
	private String Nationality;

	public Staff() {
		super();
	}
	public Staff(String Name, String LastName, String Nationality)
	{
		this.Name = Name;
		this.LastName = LastName;
		this.Nationality = Nationality;
	}
	public Integer getId() {
		return code;
	}
	public void setId(Integer id) {
		code = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getNationality() {
		return Nationality;
	}
	public void setNazionality(String nationality) {
		Nationality = nationality;
	}	

}

