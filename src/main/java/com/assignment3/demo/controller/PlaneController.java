package com.assignment3.demo.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.assignment3.demo.model.Hostess;
import com.assignment3.demo.model.Pilot;
import com.assignment3.demo.model.Plane;
import com.assignment3.demo.repository.HostessRepository;
import com.assignment3.demo.repository.PilotRepository;
import com.assignment3.demo.repository.PlaneRepository;

@Controller
public class PlaneController {


	@Autowired
	PlaneRepository planerepository;
	
	@Autowired
	PilotRepository pilotrepository;
	
	@Autowired
	HostessRepository hostessrepository;

	@RequestMapping("/infoplane/{id}")
	public String infoPlane(@PathVariable Integer id, Model model) {
		model.addAttribute("plane", planerepository.findByCode(id));
		return "infoplane";

	}
	
	@RequestMapping("/infoplaneAdmin/{id}")
	public String infoPlaneAdmin(@PathVariable Integer id, Model model) {
		model.addAttribute("plane", planerepository.findByCode(id));
		return "infoplaneAdmin";

	}

	@RequestMapping("/updatePlane/{id}")
	public String updatePlane(@PathVariable Integer id, Model model) {
		model.addAttribute("plane", planerepository.findByCode(id));
		return "updatePlane";
	}
	
	@RequestMapping(value="/updatePlane/{id}", method=RequestMethod.POST)
	public String updatePlane(
			@PathVariable Integer id,
			@RequestParam String type, @RequestParam String numberseats,
			Model model){
		Plane plane = planerepository.findByCode(id);
		plane.setType(type);
		plane.setNumberSeats(Integer.parseInt(numberseats));
		planerepository.save(plane);
		return "redirect:/homeAdmin/";
	}
	
	@RequestMapping("/updatePlanePilot/{id}")
	public String updatePilotonPlane(@PathVariable Integer id, Model model) {
		model.addAttribute("plane", planerepository.findByCode(id));
		model.addAttribute("pilots", pilotrepository.findAll());
		return "updatePlanePilot";
	}
	
	@RequestMapping(value ="/updatePilotOnPlane/{id1}/{id2}")
	public String updatePilotonPlane(@PathVariable Integer id1, @PathVariable Integer id2) {
		Plane plane = planerepository.findByCode(id1);
		Pilot newpilot = pilotrepository.findByCode(id2);
		Pilot oldPilot = plane.getCaptains();
		oldPilot.getPlanes().remove(plane);
		newpilot.getPlanes().add(plane);
		pilotrepository.save(oldPilot);
		pilotrepository.save(newpilot);
		plane.setCaptains(newpilot);
		planerepository.save(plane);
		return "redirect:/updatePlane/{id1}";
	}
	@RequestMapping("/updatePlaneHostess/{id1}")
	public String updateHostessonPlane(@PathVariable Integer id1, Model model) {
		model.addAttribute("plane", planerepository.findByCode(id1));
		model.addAttribute("hostess", hostessrepository.findAll());
		return "updatePlaneHostess";
	}
	
	@RequestMapping("/addHostesOnPlane/{id1}/{id2}")
	public String updateHostToPlane(@PathVariable Integer id1, @PathVariable Integer id2) {
		Plane plane = planerepository.findByCode(id1);
		Hostess hostess = hostessrepository.findByCode(id2);
		if (plane.getHostess().contains(hostess)) {
			plane.getHostess().remove(hostess);
			planerepository.save(plane);
			hostess.getPlanes().remove(plane);
			hostessrepository.save(hostess);
		}
		else {
			plane.getHostess().add(hostess);
			planerepository.save(plane);
			hostess.getPlanes().add(plane);
			hostessrepository.save(hostess);
		}	
		return "redirect:/updatePlaneHostess/{id1}/";
	}
	
}
