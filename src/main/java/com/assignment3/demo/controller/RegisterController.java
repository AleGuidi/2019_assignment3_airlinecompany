package com.assignment3.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.assignment3.demo.model.Airport;
import com.assignment3.demo.model.Hostess;
import com.assignment3.demo.model.Pilot;
import com.assignment3.demo.model.Plane;
import com.assignment3.demo.model.Register;
import com.assignment3.demo.repository.AirportRepository;
import com.assignment3.demo.repository.HostessRepository;
import com.assignment3.demo.repository.PilotRepository;
import com.assignment3.demo.repository.PlaneRepository;
import com.assignment3.demo.repository.RegisterRepository;

@Controller
public class RegisterController {


	@Autowired
	RegisterRepository registerrepository;

	@Autowired
	AirportRepository airportrepository;
	
	@Autowired
	PlaneRepository planerepository;
	
	@Autowired
	HostessRepository hostessrepository;
	
	@Autowired
	PilotRepository pilotrepository;

	@RequestMapping(value = "/homeAdmin", method = RequestMethod.GET)
	public String ViewHomeAdmin(Model model)
	{
		model.addAttribute("Register", registerrepository.findAll());
		return "homeAdmin";
	}

	@RequestMapping(value = "/homePilot", method = RequestMethod.GET)
	public String ViewHomePilot(Model model)
	{
		model.addAttribute("Register", registerrepository.findAll());
		return "homePilot";
	}

	@RequestMapping(value = "/onlyread", method = RequestMethod.GET)
	public String ViewHome(Model model)
	{
		model.addAttribute("Register", registerrepository.findAll());
		return "home";
	}
	
	@RequestMapping(value = "/Addrouteview", method = RequestMethod.GET)
	public String AddRouteview(Model model) {
		model.addAttribute("airports", airportrepository.findAll());
		model.addAttribute("planes",planerepository.findAll());
		return "Addrouteview";
	}

	@RequestMapping(value="/newRoute", method=RequestMethod.POST)
	public String newRoute( 
			@RequestParam String dataPartenza, @RequestParam String orarioPartenza,
			@RequestParam String dataArrivo, @RequestParam String orarioArrivo, 
			Model model) {
		Register newRoute = new Register();
		newRoute.setDepartureDate(dataPartenza);
		newRoute.setDepartureTime(orarioPartenza);
		newRoute.setArrivalDate(dataArrivo);
		newRoute.setArrivalTime(orarioArrivo);
		registerrepository.save(newRoute);
		Integer id = newRoute.getCode();
		model.addAttribute("Register",newRoute);
		return "redirect:/Addroute/"+id;
	}
	
	@RequestMapping(value = "/Addroute/{id}")
	public String addRoute(@PathVariable Integer id, Model model) {
		model.addAttribute("airports",airportrepository.findAll());
		model.addAttribute("planes", planerepository.findAll());
		model.addAttribute("register", registerrepository.findByCode(id));
		return "Addroute";
	}
	
	@RequestMapping(value="/addDeparture{id1}/{id2}", method = RequestMethod.GET)
	public String addDeparture(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register = registerrepository.findByCode(id1);
		Airport partenza = airportrepository.findByCode(id2);
		register.getAirports().add(partenza);
		partenza.getRegisters().add(register);
		registerrepository.save(register);
		airportrepository.save(partenza);
		return "redirect:/Addroute/{id1}";	
	}
	
	@RequestMapping(value="/addArrival{id1}/{id2}", method = RequestMethod.GET)
	public String addArrival(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register = registerrepository.findByCode(id1);
		Airport destinazione = airportrepository.findByCode(id2);
		register.getAirports().add(destinazione);
		destinazione.getRegisters().add(register);
		registerrepository.save(register);
		airportrepository.save(destinazione);
		return "redirect:/Addroute/{id1}";
	}
	
	@RequestMapping(value="/addPlaneOnRoute{id1}/{id2}", method = RequestMethod.GET)
	public String addPlaneOnPlane(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register = registerrepository.findByCode(id1);
		Plane plane = planerepository.findByCode(id2);
		register.setPlane(plane);
		plane.getRegisters().add(register);
		registerrepository.save(register);
		planerepository.save(plane);
		return "redirect:/Addroute/{id1}";	
	}
	
	@RequestMapping(value = "/addPlane/{id1}/{id2}",method = RequestMethod.GET)
	public String addPlane(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register = registerrepository.findByCode(id1);
		Plane plane = planerepository.findByCode(id2);
		register.setPlane(plane);
		plane.getRegisters().add(register);
		registerrepository.save(register);
		planerepository.save(plane);
		return "homeAdmin";
	}

	@RequestMapping(value = "/updateRegister/{id}")
	public String updateRegister(@PathVariable Integer id, Model model) {
		model.addAttribute("Register", registerrepository.findByCode(id));
		return "updateRegister";
	}
	
	@RequestMapping(value = "/updateRegisterAdmin/{id}")
	public String updateRegisterAdmin(@PathVariable Integer id, Model model) {
		model.addAttribute("Register", registerrepository.findByCode(id));
		return "updateRegisterAdmin";
	}
	
	@RequestMapping(value="/UpdateRegister/{id}", method=RequestMethod.POST)
	public String updateRegister(
			@PathVariable Integer id,
			@RequestParam String dataPartenza, @RequestParam String orarioPartenza,
			@RequestParam String dataArrivo, @RequestParam String orarioArrivo,
			Model model){
		Register register= registerrepository.findByCode(id);
		register.setDepartureDate(dataPartenza);
		register.setDepartureTime(orarioPartenza);
		register.setArrivalDate(dataArrivo);
		register.setArrivalTime(orarioArrivo);
		registerrepository.save(register);
			return "redirect:/homePilot/";	
	}
	@RequestMapping(value="/UpdateRegisterAdmin/{id}", method=RequestMethod.POST)
	public String updateRegisterAdmin(
			@PathVariable Integer id,
			@RequestParam String dataPartenza, @RequestParam String orarioPartenza,
			@RequestParam String dataArrivo, @RequestParam String orarioArrivo,
			Model model){
		Register register= registerrepository.findByCode(id);
		register.setDepartureDate(dataPartenza);
		register.setDepartureTime(orarioPartenza);
		register.setArrivalDate(dataArrivo);
		register.setArrivalTime(orarioArrivo);
		registerrepository.save(register);
			return "redirect:/homeAdmin/";	
	}
	
	@RequestMapping(value = "/updateRegisterAirports/{id}")
	public String updateAirportsOnRegiter(@PathVariable Integer id, Model model) {
		model.addAttribute("register", registerrepository.findByCode(id));
		model.addAttribute("Airports", airportrepository.findAll());
		return "UpdateRegisterAirports";
	}
	
	@RequestMapping(value = "/updateRegisterAirportsAdmin/{id}")
	public String updateAirportsOnRegiterAdmin(@PathVariable Integer id, Model model) {
		model.addAttribute("register", registerrepository.findByCode(id));
		model.addAttribute("Airports", airportrepository.findAll());
		return "UpdateRegisterAirportsAdmin";
	}
	@RequestMapping(value ="/updateArrival/{id1}/{id2}")
	public String updateArrivalOnRegister(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register =registerrepository.findByCode(id1);
		Airport arrival =airportrepository.findByCode(id2);
		Airport departure = register.getAirports().get(0);
		Airport a = register.getAirports().get(1);
		a.getRegisters().remove(register);
		arrival.getRegisters().add(register);
		airportrepository.save(a);
		airportrepository.save(arrival);
		List<Airport> airports = new ArrayList<Airport>();
		airports.add(departure);
		airports.add(arrival);
		register.setAirports(airports);
		registerrepository.save(register);
		return "redirect:/updateRegister/{id1}";
	}
	@RequestMapping(value ="/updateArrivalAdmin/{id1}/{id2}")
	public String updateArrivalOnRegisterAdmin(@PathVariable Integer id1, @PathVariable Integer id2) {
		Register register =registerrepository.findByCode(id1);
		Airport arrival =airportrepository.findByCode(id2);
		Airport departure = register.getAirports().get(0);
		Airport a = register.getAirports().get(1);
		a.getRegisters().remove(register);
		arrival.getRegisters().add(register);
		airportrepository.save(a);
		airportrepository.save(arrival);
		List<Airport> airports = new ArrayList<Airport>();
		airports.add(departure);
		airports.add(arrival);
		register.setAirports(airports);
		registerrepository.save(register);
		return "redirect:/updateRegisterAdmin/{id1}";
	}
	
	@RequestMapping(value ="/DeleteRoute/{idreg}", method = RequestMethod.GET)
	public String DeleteRoute(@PathVariable Integer idreg) {
		Register register = registerrepository.findByCode(idreg);
		
		List<Airport> airports = register.getAirports();
		Plane plane = register.getPlane();
		for(Airport a:airports) {
			a.getRegisters().remove(register);
		}
		plane.getRegisters().remove(register);
		registerrepository.delete(register);
		return "redirect:/homeAdmin/";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String Search(@RequestParam String idsearch, Model model) {
		List<Airport> airports = (List<Airport>) airportrepository.findAll();
		List<Hostess> hostesses = (List<Hostess>) hostessrepository.findAll();
		List<Pilot> pilots = (List<Pilot>) pilotrepository.findAll();
		List<Plane> planes = (List<Plane>) planerepository.findAll();
		List<Register> registers = (List<Register>) registerrepository.findAll();
		
		//search on airports attribute
		List<Airport> airportsname = new ArrayList<Airport>();
		List<Airport> airportscity = new ArrayList<Airport>();
		List<Airport> airportsnation = new ArrayList<Airport>();
		for(Airport a:airports) {
			if(a.getName().toLowerCase().equals(idsearch.toLowerCase()))
					airportsname.add(a);
			if(a.getCity().toLowerCase().equals(idsearch.toLowerCase()))
					airportscity.add(a);
			if(a.getNation().toLowerCase().equals(idsearch.toLowerCase()))
					airportsnation.add(a);
		}
		//search on hostess attribute
		List<Hostess> hostessesname = new ArrayList<Hostess>();
		List<Hostess> hostesseslastname = new ArrayList<Hostess>();
		List<Hostess> hostessesnationality = new ArrayList<Hostess>();
		for(Hostess h:hostesses) {
			if(h.getName().toLowerCase().equals(idsearch.toLowerCase()))
				hostessesname.add(h);
			if(h.getLastName().toLowerCase().equals(idsearch.toLowerCase()))
				hostesseslastname.add(h);
			if(h.getNationality().toLowerCase().equals(idsearch.toLowerCase()))
				hostessesnationality.add(h);
		}
		//search on pilot attribute
		List<Pilot> pilotname = new ArrayList<Pilot>();
		List<Pilot> pilotlastname = new ArrayList<Pilot>();
		List<Pilot> pilotnationality = new ArrayList<Pilot>();
		List<Pilot> pilotyearservice = new ArrayList<Pilot>();
		List<Pilot> pilottype = new ArrayList<Pilot>();
		for(Pilot p:pilots) {
			if(p.getName().toLowerCase().equals(idsearch.toLowerCase()))
				pilotname.add(p);
			if(p.getLastName().toLowerCase().equals(idsearch.toLowerCase()))
				pilotlastname.add(p);
			if(p.getNationality().toLowerCase().equals(idsearch.toLowerCase()))
				pilotnationality.add(p);
			if(p.getYearsService().toString().toLowerCase().equals(idsearch.toLowerCase()))
				pilotyearservice.add(p);
			if(p.getTypeAircraft().toLowerCase().equals(idsearch.toLowerCase()))
				pilottype.add(p);
		}
		//search on plane attribute
		List<Plane> planetype = new ArrayList<Plane>();
		List<Plane> planenumseat = new ArrayList<Plane>();
		for(Plane pl:planes) {
			if(pl.getType().toLowerCase().equals(idsearch.toLowerCase()))
				planetype.add(pl);
			if(pl.getNumberSeats().toString().toLowerCase().equals(idsearch.toLowerCase()))
				planenumseat.add(pl);
		}
		//search on register attribute
		List<Register> registerddate = new ArrayList<Register>();
		List<Register> registerdtime = new ArrayList<Register>();
		List<Register> registeradate = new ArrayList<Register>();
		List<Register> registeratime = new ArrayList<Register>();
		for (Register r : registers) {
			if(r.getDepartureDate().equals(idsearch))
				registerddate.add(r);
			if(r.getDepartureTime().equals(idsearch))
				registerdtime.add(r);
			if(r.getArrivalDate().equals(idsearch))
				registeradate.add(r);
			if(r.getArrivalTime().equals(idsearch))
				registeratime.add(r);
		}
		model.addAttribute("airportsname", airportsname);
		model.addAttribute("airportscity", airportscity);
		model.addAttribute("airportsnation", airportsnation);
		
		model.addAttribute("hostessesname", hostessesname);
		model.addAttribute("hostesseslastname", hostesseslastname);
		model.addAttribute("hostessesnationality", hostessesnationality);
		
		model.addAttribute("pilotname", pilotname);
		model.addAttribute("pilotlastname", pilotlastname);
		model.addAttribute("pilotnationality", pilotnationality);
		model.addAttribute("pilotyearservice", pilotyearservice);
		model.addAttribute("pilottype", pilottype);
		
		model.addAttribute("planetype", planetype);
		model.addAttribute("planenumseat", planenumseat);
		
		model.addAttribute("registerddate", registerddate);
		model.addAttribute("registerdtime", registerdtime);
		model.addAttribute("registeradate", registeradate);
		model.addAttribute("registeratime", registeratime);
		
		return "resultsearch";
		
	}

	
	


}
