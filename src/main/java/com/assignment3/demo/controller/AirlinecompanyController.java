package com.assignment3.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.assignment3.demo.repository.*;

@Controller
public class AirlinecompanyController<route> {

	@Autowired
	RegisterRepository repository;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		return "index";
	}

	@RequestMapping(value = "/Severalhome", method = RequestMethod.POST)
	public String home(@RequestParam(value = "username", required=false) String username, @RequestParam(value="password", required=false) String password) {
		if("admin".equals(username) && "admin".equals(password))
		{
			return "redirect:/homeAdmin/";
		}
		else if("pilot".equals(username) && "pilot".equals(password))
		{
			return "redirect:/homePilot/";
		}
		else
		{
			return "redirect:/Errorelogin/";
		}

	}
	
	@RequestMapping(value = "/Errorelogin", method = RequestMethod.GET)
	public String ErrorLogin(Model model) {
		return "Errorelogin";
	}



	


}
