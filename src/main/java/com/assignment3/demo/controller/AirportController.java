package com.assignment3.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.assignment3.demo.model.Airport;
import com.assignment3.demo.repository.AirportRepository;

@Controller
public class AirportController {

	@Autowired
	AirportRepository airportRepository;
	
	@RequestMapping("/infoairport/{id}")
	public String infoAirport(@PathVariable Integer id, Model model) {
		model.addAttribute("airport", airportRepository.findByCode(id));
		return "infoairport";
	}
	
	@RequestMapping(value ="/viewAllAirport", method = RequestMethod.GET)
	public String ViewAllAirport(Model model) {
		model.addAttribute("airports", airportRepository.findAll());
		return "viewAirports";
	}
	
	@RequestMapping(value ="/viewAllAirportAdmin", method = RequestMethod.GET)
	public String ViewAllAirportAdmin(Model model) {
		model.addAttribute("airports", airportRepository.findAll());
		return "viewAirportsAdmin";
	}
	
	@RequestMapping(value = "/AddAirportView", method = RequestMethod.GET)
	public String AddAirportView(Model model) {
		model.addAttribute("airports", airportRepository.findAll());
		return "AddNewAirportsView";
	}
	
	@RequestMapping(value="/newAirport", method=RequestMethod.POST)
	public String newAirport( 
			@RequestParam String name, @RequestParam String nation,
			@RequestParam String city,Model model) {
		Airport newAirport = new Airport();
		newAirport.setName(name);
		newAirport.setNation(nation);
		newAirport.setCity(city);
		airportRepository.save(newAirport);
		Integer id = newAirport.getCode();
		model.addAttribute("Register",newAirport);
		return "redirect:/AddAirport/"+id;
	}
	
	@RequestMapping(value = "/AddAirport/{id}")
	public String addRoute(@PathVariable Integer id, Model model) {
		model.addAttribute("airports",airportRepository.findAll());
		model.addAttribute("air", airportRepository.findByCode(id));
		return "AddAirport";
	}
	
	@RequestMapping(value="/addDestination{id1}/{id2}", method = RequestMethod.GET)
	public String addDeparture(@PathVariable Integer id1, @PathVariable Integer id2) {
		Airport airport = airportRepository.findByCode(id1);
		Airport destinazione = airportRepository.findByCode(id2);
		airport.setDeparture(airport);
		airportRepository.save(airport);
		airport.getDestinations().add(destinazione);
		airportRepository.save(airport);
		return "redirect:/AddAirport/{id1}";	
	}
}
