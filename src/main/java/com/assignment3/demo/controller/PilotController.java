package com.assignment3.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.assignment3.demo.repository.PilotRepository;

@Controller
public class PilotController {


	@Autowired
	PilotRepository pilotrepository;

	
	@RequestMapping("/infoPilot/{id}")
	public String infoPlane(@PathVariable Integer id, Model model) {
		model.addAttribute("pilot", pilotrepository.findById(id).get());
		return "infoPilot";

	}
	
	@RequestMapping("/updatePilot/{id1}")
	public String updatePlane(@PathVariable Integer id, Model model) {
		model.addAttribute("pilot", pilotrepository.findById(id).get());
		return "redirect:/updatePilot/{id1}";

	}
	

}
