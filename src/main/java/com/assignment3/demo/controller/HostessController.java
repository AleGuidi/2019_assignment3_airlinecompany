package com.assignment3.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.assignment3.demo.repository.HostessRepository;


@Controller
public class HostessController {


	@Autowired
	HostessRepository hostessrepository;

	
	@RequestMapping("/infoHostess/{id}")
	public String infoHostss(@PathVariable Integer id, Model model) {
		model.addAttribute("hostess", hostessrepository.findById(id).get());
		return "infoHostess";

	}
	
	@RequestMapping("/updateHostess/{id1}")
	public String updateHostess(@PathVariable Integer id, Model model) {
		model.addAttribute("hostess", hostessrepository.findById(id).get());
		return "redirect:/updateHostess/{id1}";

	}
	
}