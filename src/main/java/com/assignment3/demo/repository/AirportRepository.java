package com.assignment3.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignment3.demo.model.Airport;

public interface AirportRepository extends CrudRepository<Airport, Integer>{
	public Airport findByName(String name);
	public Airport findByCode(Integer code);

}
