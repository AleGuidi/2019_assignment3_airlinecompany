package com.assignment3.demo.repository;

import org.springframework.stereotype.Repository;

import com.assignment3.demo.model.Pilot;

//import java.util.Optional;

import org.springframework.data.repository.CrudRepository;;

@Repository
public interface PilotRepository extends CrudRepository<Pilot, Integer> {
	public Pilot findByCode(Integer code);
	
}