package com.assignment3.demo.repository;



import org.springframework.data.repository.CrudRepository;


import com.assignment3.demo.model.Register;

public interface RegisterRepository extends CrudRepository<Register, Integer> {
	public Register findByCode(Integer code);
}
