package com.assignment3.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assignment3.demo.model.Hostess;

@Repository
public interface HostessRepository extends CrudRepository<Hostess, Integer>{
	public Hostess findByCode(Integer code);

}
