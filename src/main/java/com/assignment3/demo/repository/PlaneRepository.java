package com.assignment3.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignment3.demo.model.Plane;

public interface PlaneRepository extends CrudRepository<Plane, Integer>{
	public Plane findByCode(Integer code);
}
