package com.assignment3.demo;

import com.assignment3.demo.repository.*;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.*;

import com.assignment3.demo.model.Airport;
import com.assignment3.demo.model.Hostess;
import com.assignment3.demo.model.Pilot;
import com.assignment3.demo.model.Plane;
import com.assignment3.demo.model.Register;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	PilotRepository pilotrepository;

	@Autowired
	HostessRepository hostessrepository;

	@Autowired
	PlaneRepository planerepository;

	@Autowired
	AirportRepository airportrepository;

	@Autowired
	RegisterRepository registerrepository;	


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {

		Pilot pilot1 = new Pilot("Alessandro", "Guidi", "Italiana", 10, "Airbus");
		Pilot pilot3 = new Pilot("Giuseppe", "Verdi", "Italiana", 12, "Airbus");
		Pilot pilot4 = new Pilot("Chiara", "Alini", "Italiana", 14, "Boeing");
		pilotrepository.save(pilot1);
		//pilotrepository.save(pilot2);
		pilotrepository.save(pilot3);
		pilotrepository.save(pilot4);
		
		Hostess h1 = new Hostess("Daniela", "Modica", "Italiana");
		Hostess h2 = new Hostess("Samuel", "Delson", "America");
		Hostess h3 = new Hostess("Helen", "Atena", "Greca");
		Hostess h4 = new Hostess("Maria", "Garcia", "Spagnola");
		hostessrepository.save(h1);
		hostessrepository.save(h2);
		hostessrepository.save(h3);
		hostessrepository.save(h4);
		
		Plane plane1 = new Plane("airbus", 700, pilot1 , Arrays.asList(h1,h2));
		Plane plane2 = new Plane("airbus", 650, pilot4, Arrays.asList(h3,h2));
		Plane plane3 = new Plane("boeing", 500, pilot3, Arrays.asList(h3,h4));
		Plane plane4 = new Plane("boeing", 550, pilot1, Arrays.asList(h3,h4,h1,h2));
		planerepository.save(plane1);
		planerepository.save(plane2);
		planerepository.save(plane3);
		planerepository.save(plane4);
		
		pilot1.setPlanes(Arrays.asList(plane1,plane2));
		pilot3.setPlanes(Arrays.asList(plane3));
		pilot4.setPlanes(Arrays.asList(plane2));
		pilotrepository.save(pilot1);
		pilotrepository.save(pilot3);
		pilotrepository.save(pilot4);
		

		h1.setPlanes(Arrays.asList(plane1,plane2,plane4));
		h2.setPlanes(Arrays.asList(plane1,plane4,plane2));
		h3.setPlanes(Arrays.asList(plane2,plane4));
		h4.setPlanes(Arrays.asList(plane3,plane4));	
		hostessrepository.save(h1);
		hostessrepository.save(h2);
		hostessrepository.save(h3);
		hostessrepository.save(h4);

		Airport airport1 = new Airport("Malpensa", "Italia", "Milano");
		Airport airport2 = new Airport("Linate", "Italia", "Milano");
		Airport airport3 = new Airport("Heathrow", "Inghilterra", "Londra");
		Airport airport4 = new Airport("Vancouver International", "America", "Vancouver");
		Airport airport5 = new Airport( "Chek Lap Kok", "Hong Kong", "Hong Kong");
		airportrepository.save(airport1);
		airportrepository.save(airport2);
		airportrepository.save(airport3);
		airportrepository.save(airport4);
		airportrepository.save(airport5);
		
		airport1.setDeparture(airport1);
		airport2.setDeparture(airport2);
		airport3.setDeparture(airport3);
		airport4.setDeparture(airport4);
		airport5.setDeparture(airport5);
		airportrepository.save(airport1);
		airportrepository.save(airport2);
		airportrepository.save(airport3);
		airportrepository.save(airport4);
		airportrepository.save(airport5);

		airport1.setDestinations(Arrays.asList(airport3,airport4,airport5));
		airport2.setDestinations(Arrays.asList(airport3,airport4,airport5));
		airport3.setDestinations(Arrays.asList(airport4,airport5,airport1,airport2));
		airport4.setDestinations(Arrays.asList(airport5,airport1,airport2,airport3));
		airport5.setDestinations(Arrays.asList(airport3,airport4,airport1,airport2));
		airportrepository.save(airport1);
		airportrepository.save(airport2);
		airportrepository.save(airport3);
		airportrepository.save(airport4);
		airportrepository.save(airport5);


		Register r1 = new Register("2020-07-07", "07:07", "2020-07-07", "09:08", plane1, Arrays.asList(airport1,airport3));
		Register r2 = new Register("2020-07-07", "10:00", "2020-07-07", "11:37", plane2, Arrays.asList(airport2,airport3));
		Register r6 = new Register("2020-07-08", "15:30", "2020-07-08", "18:00", plane1, Arrays.asList(airport3,airport1));
		Register r3 = new Register("2020-07-08", "07:07", "2020-07-08", "09:08", plane1, Arrays.asList(airport1,airport3));
		Register r4 = new Register("2020-07-08", "18:25", "2020-07-08", "19:56", plane3, Arrays.asList(airport4,airport5));
		Register r5 = new Register("2020-07-08", "20:00", "2020-07-08", "20:30", plane4, Arrays.asList(airport5,airport1));
		Register r7 = new Register("2020-07-10", "23:00", "2020-07-11", "01:17", plane2, Arrays.asList(airport3,airport5));
		Register r8 = new Register("2020-07-12", "10:50", "2020-07-12", "11:58", plane4, Arrays.asList(airport2,airport5));
		registerrepository.save(r1);
		registerrepository.save(r2);
		registerrepository.save(r3);
		registerrepository.save(r4);
		registerrepository.save(r5);
		registerrepository.save(r6);
		registerrepository.save(r7);
		registerrepository.save(r8);

		airport1.setRegisters(Arrays.asList(r1,r6,r3,r5));
		airport2.setRegisters(Arrays.asList(r2,r7));
		airport3.setRegisters(Arrays.asList(r1,r2,r6,r3,r8));
		airport4.setRegisters(Arrays.asList(r4));
		airport5.setRegisters(Arrays.asList(r4,r5,r7,r8));
		airportrepository.save(airport1);
		airportrepository.save(airport2);
		airportrepository.save(airport3);
		airportrepository.save(airport4);
		airportrepository.save(airport5);
		
		plane1.setRegisters(Arrays.asList(r1,r6,r3));
		plane2.setRegisters(Arrays.asList(r2,r7));
		plane3.setRegisters(Arrays.asList(r4));
		plane4.setRegisters(Arrays.asList(r5,r8));
		planerepository.save(plane1);
		planerepository.save(plane2);
		planerepository.save(plane3);
		planerepository.save(plane4);
	}

}
