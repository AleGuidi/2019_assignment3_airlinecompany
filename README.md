# 2019_assignment3_AirlineCompany

**Componenti gruppo**
*  Chiara Alini 861204
*  Alessandro Guidi 808065

**Ambiente di sviluppo**
*  Ecliplse: con pacchetto *Maven (Java EE) Integration* per le dipendenze maven
*  Spring version 2.2.2.RELEASE
*  java version1.8
*  h2database


**Requisiti**
*  JDK java 13.0.1
*  maven apache 3.6.3

**Come avviarla**
1.  Scaricare la repository dal link : 
    [https://gitlab.com/AleGuidi/2019_assignment3_airlinecompany/](url)
2.  Entrare nella repository *2019_assignment3_airlinecompany*
3.  avviare il comando `mvc spring:boot run` da linea di comando
4.  Aprire una pagina web all'indirizzo `localhost:8080`
5.  Per i diversi accessi è possibile consultare la relazione relativa all'applicazione web nella repository indicata precedentemente